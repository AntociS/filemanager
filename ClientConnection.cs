﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Threading;

namespace FileManagerV1._0
{
    class ClientConnection
    {
        private Socket newClient;
        private string GuestPassword;
        private string AdminPassword;
        private Thread CommWithClient;
        private int nrOfAttempts;
        private ManageFiles ServerFiles;
        private int StopCommunication;
        private List<string> DirContent;

        //This class will handle client connection
        public ClientConnection(Socket Client)
        {
            newClient = Client;
            ServerFiles = new ManageFiles();
            DirContent = new List<string>();
            GuestPassword = "1234";
            AdminPassword = "12345";
            CommWithClient = new Thread(DialogWithClient);
            nrOfAttempts = 0;
            StopCommunication = 0;
        }

        public void Start()
        {
            CommWithClient.Start();
        }

        public int CheckPassword(string pass)
        {
            //Guest have access to non-system drives
            if (GuestPassword == pass)
                return 1;
            //Admin have access to all drives
            else if (AdminPassword == pass)
                return 2;
            else
                return 0;
        }

        //send method
        public void Send(string message)
        {
            byte[] data = System.Text.Encoding.ASCII.GetBytes(message);
            newClient.Send(data);
        }

        //receive method
        public string Receive()
        {
            String line = "";
            try
            {
                byte[] buffer = new byte[1024];
                int aux = newClient.Receive(buffer);
                char[] msg = new char[aux];

                System.Text.Decoder d = System.Text.Encoding.UTF8.GetDecoder();
                //decoding bytes into characters
                int charLen = d.GetChars(buffer, 0, aux, msg, 0);
                //create new string from our array of chars
                line = new System.String(msg);
            }
            catch(Exception ex)
            {
                line = "Client disconnected";
            }
            return line;

        }
        
        //through this function server can communicate with client
        public void DialogWithClient()
        {

            while (nrOfAttempts < 3 && StopCommunication == 0)
            {
                //Received password from client
                string line = Receive();
                //Check if password is correct and which password is
                int AllowCommunication = CheckPassword(line);
                //Password is ok. Client is logged in as guest so he have access to non-system drives only
                if (AllowCommunication == 1)
                {
                    //Send back welcome message
                    Send("220 Success. You're logged in as Guest");
                    Thread.Sleep(100);
                    Send("!");
                }
                //Password is ok. Client is logged in as admin so he have access to all drives
                else if (AllowCommunication == 2)
                {
                    Send("220 Success. You're logged in as Administrator");
                    Thread.Sleep(10);
                    Send("!");
                }
                //Password is wrong
                else
                {
                    nrOfAttempts++;
                    //If client sent 3 times wrong password he will be disconnected
                    if (nrOfAttempts < 3)
                    {
                        Send(String.Format("222 You have {0} attempts", 3 - nrOfAttempts));
                        Thread.Sleep(100);
                        Send("!");
                    }
                }
                //Password is ok and nobody want to close connection. So we can start sending data
                while(AllowCommunication != 0 && StopCommunication == 0)
                {
                    line = Receive();
                    string response = "";
                    //We will @ to split commands from arguments
                    string[] command = line.Split('@');
                    string cmd = command[0].ToUpperInvariant();
                    string[] arguments = null;
                    if (command.Length == 1)
                    {
                        arguments = new string[1];
                        arguments[0] = "";
                    }
                    else if (command.Length == 2)
                    {
                        arguments = new string[1];
                        arguments[0] = command[1];
                    }
                    else if (command.Length == 3)
                    {
                        arguments = new string[2];
                        arguments[0] = command[1];
                        arguments[1] = command[2];
                    }
                    else if (command.Length == 4)
                    {
                        arguments = new string[3];
                        arguments[0] = command[1];
                        arguments[1] = command[2];
                        arguments[2] = command[3];
                    }

                    switch (cmd)
                    {
                        //list all files from folder specified in arguments[0]
                        case "LIST":
                            //Call function from class which manages local files
                            DirContent = ServerFiles.ShowContent(arguments[0]);
                            for (int i = 0; i < DirContent.Count; i++)
                            {
                                //Check if client is logged in as guest or admin.
                                if (AllowCommunication == 1)
                                {
                                    //Send just non-system drives
                                    if (DirContent[i] != Path.GetPathRoot(Environment.SystemDirectory))
                                    {
                                        Send(DirContent[i]);
                                        Thread.Sleep(10);
                                    }
                                }
                                else if (AllowCommunication == 2)
                                {
                                    Send(DirContent[i]);
                                    Thread.Sleep(10);
                                }
                            }
                            Thread.Sleep(10);
                            Send("!");
                            break;

                            //this command informs server that client wants to close connection
                        case "QUIT":
                            //Send response back
                            response = "221 Disconnecting";
                            Send(response);
                            Thread.Sleep(10);
                            Send("!");
                            break;

                            //Copy command
                        case "COPY":
                            //set source directory, relative file name, and destination directory
                            ServerFiles.SetSrcDirectoryRemote(arguments[0]);
                            ServerFiles.SetFileNameRemote(arguments[1]);
                            ServerFiles.SetDestDirectoryRemote(arguments[2]);

                            //Call function which is performing copy/move operation on local machine
                            ServerFiles.CopyMoveOp(1, 0, 1);

                            //Send back updated folders
                            DirContent = ServerFiles.ShowContent(arguments[2]);
                            for (int i = 0; i < DirContent.Count;i++)
                            {
                                Send(DirContent[i]);
                                Thread.Sleep(10);
                            }
                            Thread.Sleep(10);
                            Send("!");
                            break;

                            //move command
                        case "MOVE":
                            //set source directory, relative file name, and destination directory
                            ServerFiles.SetSrcDirectoryRemote(arguments[0]);
                            ServerFiles.SetFileNameRemote(arguments[1]);
                            ServerFiles.SetDestDirectoryRemote(arguments[2]);

                            //Call function which is performing copy/move operation on local machine
                            ServerFiles.CopyMoveOp(0, 1, 1);

                            //Send back updated folders
                            DirContent = ServerFiles.ShowContent(arguments[2]);
                            for (int i = 0; i < DirContent.Count; i++)
                            {
                                Send(DirContent[i]);
                                Thread.Sleep(10);
                            }
                            Thread.Sleep(10);
                            Send("!");
                            break;

                        case "CRDI": // CREATE DIRECTORY
                            //Call function which creates directory in specified directory on local machine
                            ServerFiles.AddDirectory(arguments[0], arguments[1]);
                            DirContent = ServerFiles.ShowContent(arguments[0]);
                            for (int i = 0; i < DirContent.Count; i++)
                            {
                                Send(DirContent[i]);
                                Thread.Sleep(10);
                            }
                            Send("!");
                            break;
                        case "CRFL": //CREATE FILE
                            //Call function which creates files in specified directory on local machine
                            ServerFiles.AddFile(arguments[0], arguments[1]);
                            DirContent = ServerFiles.ShowContent(arguments[0]);
                            for (int i = 0; i < DirContent.Count; i++)
                            {
                                Send(DirContent[i]);
                                Thread.Sleep(10);
                            }
                            Send("!");
                            break;
                        case "DELT": //DELETE DIRECTORY
                            ServerFiles.DeleteOp(Path.Combine(arguments[0],arguments[1]));
                            DirContent = ServerFiles.ShowContent(arguments[0]);
                            for (int i = 0; i < DirContent.Count;i++)
                            {
                                Send(DirContent[i]);
                                Thread.Sleep(10);
                            }
                            Thread.Sleep(10);
                            Send("!");
                            break;
                        case "RENM": //RENAME FILE || DIRECTORY
                            ServerFiles.RenameOp(arguments[0], arguments[1], arguments[2]);
                            DirContent = ServerFiles.ShowContent(arguments[0]);
                            for (int i = 0; i < DirContent.Count; i++)
                            {
                                Send(DirContent[i]);
                                Thread.Sleep(10);
                            }
                            Thread.Sleep(10);
                            Send("!");
                            break;
                        default:
                            StopCommunication = 1;
                            break;

                    }
                    //If we send back message starting with 221 it means that we are closing connection
                    if (response.StartsWith("221"))
                    {
                        StopCommunication = 1;
                        break;
                    } 
                }
            }
            //Client send wrong password too many times
            if(nrOfAttempts == 3)
            {
                Send("404 You have entered wrong password too many times. Disconnecting...");
                Thread.Sleep(10);
                Send("!");
            }
            //delete specified client from list of clients
            Server.DeleteClientFromList(newClient);
            Thread.Sleep(1000);
            //close connection with client
            newClient.Close();
        }
    }
}
