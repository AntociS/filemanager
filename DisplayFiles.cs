﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.IO;

namespace FileManagerV1._0
{
    //This class implements all functions intended for communication with user.  
    public partial class DisplayFiles : Form
    {
        private List<string> ContentToDisplay;
        //instantiate class which manages local files
        private ManageFiles LocalFiles;
        //instantiate class which communicates with clients connected to my computer
        private Server ShareFiles;
        //instantiate class which communicates with remote server
        private Client FilesFromRemoteConn;
        //parent folders
        private string parentFolder1;
        private string parentFolder2;
        //Server thread
        private Thread ServerThread;

        private int ChooseListbox;
        //flags for copy/move operations
        private int CopyFlag;
        private int MoveFlag;
        //Flag for local mode/ remote mode
        private int ClientMode;

        public DisplayFiles()
        {
            InitializeComponent();

            //handlerele de la interfata grafica
            listBox1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(Double_ClickLbt1);
            listBox2.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(Double_ClickLbt2);
            listBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(Click_Lbt1);
            listBox2.MouseClick += new System.Windows.Forms.MouseEventHandler(Click_Lbt2);
            listBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(Mouse_Right_Button1);
            listBox2.MouseUp += new System.Windows.Forms.MouseEventHandler(Mouse_Right_Button2);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(DisplayFiles_FormClosing);

            ContentToDisplay = new List<string>();
            LocalFiles = new ManageFiles();

            //campurile in care se scrie folderul parinte a celor 2 listbox-uri
            parentFolder1 = "";
            parentFolder2 = "";

            // flag-uri utilizte in cod
            ChooseListbox = 0;
            CopyFlag = 0;
            MoveFlag = 0;
            ClientMode = 0;
        }

        private void DisplayFiles_Load(object sender, EventArgs e)
        {
            // rularea aplicatiei se afiseaza disk-urile locale
            ContentToDisplay = LocalFiles.ShowContent("");
            WriteContentToListbox();
        }

        //Display in listbox function
        private void WriteContentToListbox()
        {
            // afisare in primul listbox
            if (ChooseListbox == 1)
            {
                listBox1.Items.Clear();
                for (int i = 0; i < ContentToDisplay.Count;i++)
                {
                    listBox1.Items.Add(ContentToDisplay[i]);
                }
                listBox1.Items.Add("");              
            }
            //afisare al 2 listbox
            else if (ChooseListbox == 2)
            {
                listBox2.Items.Clear();
                for (int i = 0; i < ContentToDisplay.Count; i++)
                {
                    listBox2.Items.Add(ContentToDisplay[i]);
                }
                listBox2.Items.Add("");
            }
            //in cazul in care urmeaza afisarea in ambele listox-uri
            else
            {
                listBox1.Items.Clear();
                listBox2.Items.Clear();
                for (int i = 0; i < ContentToDisplay.Count; i++)
                {
                    listBox1.Items.Add(ContentToDisplay[i]);
                    listBox2.Items.Add(ContentToDisplay[i]);
                }
            }
        }

        //Right click on listbox1 handler
        private void Mouse_Right_Button1(object sender, MouseEventArgs e)
        {
            //verify if we made click on listbox1 with Right button of mouse
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                    //select listbox1
                    ChooseListbox = 1;
                    listBox1.SelectedIndex = listBox1.IndexFromPoint(e.X, e.Y);
                    int idx = listBox1.IndexFromPoint(e.Location);

                    //check if we at click location isn't any listbox item
                    if (idx == ListBox.NoMatches || listBox1.SelectedItem.ToString() == "")
                    {
                    // display menu for adding files and folders
                        NonListBoxClick.Show(Cursor.Position);
                    }
                    else
                    {
                    //display menu which allows operations with files and folders
                        OnListBoxClick.Show(Cursor.Position);
                    }
            }
        }

        // Right click on listbox2 handler
        private void Mouse_Right_Button2(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                ChooseListbox = 2;
                listBox2.SelectedIndex = listBox2.IndexFromPoint(e.X, e.Y);
                int idx = listBox2.IndexFromPoint(e.Location);  
                if (idx == ListBox.NoMatches || listBox2.SelectedItem.ToString() == "")
                {
                    NonListBoxClick.Show(Cursor.Position);
                }
                else
                {
                    OnListBoxClick.Show(Cursor.Position);
                }
            }
        }

        //handler click listbox1
        private void Click_Lbt1(object sender, MouseEventArgs e)
        {
            ChooseListbox = 1;
        }

        //handler click listbox2.
        private void Click_Lbt2(object sender, MouseEventArgs e)
        {
            ChooseListbox = 2;
        }


        //Double click on listbox1 element. Open folder, files/run executables, applications .
        private void Double_ClickLbt1(object sender, MouseEventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                // Check if we made double click on file
                    if (File.Exists(Path.Combine(parentFolder1, listBox1.SelectedItem.ToString())))
                    {
                    //check if we aren't in remote mode
                        if (ClientMode == 0)
                        {
                            System.Diagnostics.Process.Start(Path.Combine(parentFolder1, listBox1.SelectedItem.ToString()));
                        }
                    }
                    // check if we made double click on folder
                if (Directory.Exists(Path.Combine(parentFolder1, listBox1.SelectedItem.ToString())))
                {
                    parentFolder1 = Path.Combine(parentFolder1, listBox1.SelectedItem.ToString());
                    // insert in textbox new parent folder
                    txtparentFolder1.Text = parentFolder1;
                    ChooseListbox = 1;
                    ContentToDisplay.Clear();
                    //if we are connected to another client
                    if (ClientMode == 1)
                    {
                        //Send list command
                        FilesFromRemoteConn.Send("LIST@" + parentFolder1);
                        //receive message from remote machine
                        ContentToDisplay = FilesFromRemoteConn.Receive();
                        //Check if we receive a message, or folder content. Messages starts with 3 digit number.
                        if (ContentToDisplay.Count() == 1 && (ContentToDisplay[0].StartsWith("220") || ContentToDisplay[0].StartsWith("221") || ContentToDisplay[0].StartsWith("222") || ContentToDisplay[0].StartsWith("404") || ContentToDisplay[0].StartsWith("500")))
                        {
                            //Display message
                            txtFromServer.Text += ContentToDisplay[0];
                            txtFromServer.Text += Environment.NewLine;
                        }
                        else
                        {
                            //Display folder content in listbox
                            WriteContentToListbox();
                        }
                    }
                    else
                    {
                        //if we are in Local mode, we display content of selected folder
                        ContentToDisplay = LocalFiles.ShowContent(parentFolder1);
                        WriteContentToListbox();
                    }
                    
                }
            }
        }
        //Double click on listbox2 element
        private void Double_ClickLbt2(object sender, MouseEventArgs e)
        {
            if (listBox2.SelectedIndex != -1)
            {
                if (File.Exists(Path.Combine(parentFolder2, listBox2.SelectedItem.ToString())))
                {
                    if (ClientMode == 0)
                    {
                        System.Diagnostics.Process.Start(Path.Combine(parentFolder2, listBox2.SelectedItem.ToString()));
                    }
                }

                if (Directory.Exists(Path.Combine(parentFolder2, listBox2.SelectedItem.ToString())))
                {
                    parentFolder2 = Path.Combine(parentFolder2, listBox2.SelectedItem.ToString());
                    txtparentFolder2.Text = parentFolder2;
                    ChooseListbox = 2;
                    ContentToDisplay.Clear();
                    if (ClientMode == 1)
                    {
                        FilesFromRemoteConn.Send("LIST@" + parentFolder2);
                        ContentToDisplay = FilesFromRemoteConn.Receive();
                        if (ContentToDisplay.Count() == 1 && (ContentToDisplay[0].StartsWith("220") || ContentToDisplay[0].StartsWith("221") || ContentToDisplay[0].StartsWith("222") || ContentToDisplay[0].StartsWith("404") || ContentToDisplay[0].StartsWith("500")))
                        {
                            txtFromServer.Text += ContentToDisplay[0];
                            txtFromServer.Text += Environment.NewLine;
                        }
                        else
                        {
                            WriteContentToListbox();
                        }
                    }
                    else
                    {
                        ContentToDisplay = LocalFiles.ShowContent(parentFolder2);
                        WriteContentToListbox();
                    }
                }
            }
        }


        //Forward button click
        private void button4_Click(object sender, EventArgs e)
        {
            //Check if we selected just one item just from one listbox
            if ((listBox1.SelectedIndex != -1 && listBox2.SelectedIndex == -1) || (listBox2.SelectedIndex != -1 && listBox1.SelectedIndex == -1))
            {
                string SelectedDirectory;
                //selected item is from first listbox
                if (listBox1.SelectedIndex != -1)
                {
                    ChooseListbox = 1;
                    SelectedDirectory = listBox1.SelectedItem.ToString();
                    listBox1.Items.Clear();
                    //remote mode is on
                    if (ClientMode == 1)
                    {
                        //Send list command
                        FilesFromRemoteConn.Send("LIST@" + Path.Combine(parentFolder1, SelectedDirectory));
                        //Receive message or folder content
                        ContentToDisplay = FilesFromRemoteConn.Receive();
                        //Check if we received message
                        if (ContentToDisplay.Count() == 1 && (ContentToDisplay[0].StartsWith("220") || ContentToDisplay[0].StartsWith("221") || ContentToDisplay[0].StartsWith("222") || ContentToDisplay[0].StartsWith("404") || ContentToDisplay[0].StartsWith("500")))
                        {
                            //Display message inside textbox
                            txtFromServer.Text += ContentToDisplay[0];
                            txtFromServer.Text += Environment.NewLine;
                        }
                        else
                        {
                            //Display folder content inside listboxes
                            parentFolder1 = Path.Combine(parentFolder1, SelectedDirectory);
                            txtparentFolder1.Text = parentFolder1;
                            WriteContentToListbox();
                        }
                    }
                    //we are in local mode
                    else
                    {
                        ContentToDisplay = LocalFiles.ShowContent(Path.Combine(parentFolder1, SelectedDirectory));
                        //Insert new parent folder inside textbox
                        parentFolder1 = Path.Combine(parentFolder1, SelectedDirectory);
                        txtparentFolder1.Text = parentFolder1;
                        //Display folder content inside listboxes
                        WriteContentToListbox();
                    }
                }
                //Selected item is from listbox2
                else if (listBox2.SelectedIndex != -1)
                {
                    ChooseListbox = 2;
                    SelectedDirectory = listBox2.SelectedItem.ToString();
                    listBox2.Items.Clear();
                    //we are in remote mode now
                    if (ClientMode == 1)
                    {
                        //Send list command to remote machine
                        FilesFromRemoteConn.Send("LIST@" + Path.Combine(parentFolder2, SelectedDirectory));
                        //receive message
                        ContentToDisplay = FilesFromRemoteConn.Receive();
                        //Check if we received message or folder content
                        if (ContentToDisplay.Count() == 1 && (ContentToDisplay[0].StartsWith("220") || ContentToDisplay[0].StartsWith("221") || ContentToDisplay[0].StartsWith("222") || ContentToDisplay[0].StartsWith("404") || ContentToDisplay[0].StartsWith("500")))
                        {
                            //we received message. Display message in textboxes
                            txtFromServer.Text += ContentToDisplay[0];
                            txtFromServer.Text += Environment.NewLine;
                        }
                        //We received items from folder
                        else
                        {
                            // set new parent folder
                            parentFolder2 = Path.Combine(parentFolder2, SelectedDirectory);
                            txtparentFolder2.Text = parentFolder2;
                            WriteContentToListbox();
                        }
                    }
                    //We are in local mode
                    else
                    {
                        //Call function for selecting all files or folders from specified folder
                        ContentToDisplay = LocalFiles.ShowContent(Path.Combine(parentFolder2, SelectedDirectory));
                        //set new parent folder
                        parentFolder2 = Path.Combine(parentFolder2, SelectedDirectory);
                        txtparentFolder2.Text = parentFolder2;
                        //Display all the content of a folder
                        WriteContentToListbox();
                    }
                }
            }
        }


        //Back button click
        private void BackButton_Click(object sender, EventArgs e)
        {
            string aux = "";
            //Verify in which listbox was made last click. In that listbox we will display folder content
            if (ChooseListbox == 1)
            {
                aux = parentFolder1;
            }
            else if(ChooseListbox == 2)
            {
                aux = parentFolder2;
            }

            // Check if length of new parent folder is 3. In this case, we won't have parent folder because we will display drives
            if (aux.Length == 3)
            {
                aux = "";
            }
            else
            {
                //Search for last index of \\
                int lastBackSlashPos = aux.LastIndexOf('\\');
                // '\\' was not found
                if(lastBackSlashPos == -1)
                {
                }
                // if index == 2 we will display content of a drive
                else if (lastBackSlashPos == 2)
                {
                    aux = aux.Substring(0, lastBackSlashPos + 1);
                }
                // if index is bigger than 2 we will display content of a folder
                else if (lastBackSlashPos > 2)
                {
                    aux = aux.Substring(0, lastBackSlashPos);
                }
            }
            //last click was made on first listbox
            if (ChooseListbox == 1)
            {
                parentFolder1 = aux;
                txtparentFolder1.Text = parentFolder1;
                //Client mode
                if (ClientMode == 1)
                {

                    if (parentFolder1 == "")
                    {
                        // if parentFolder is equal to "",we will send list command without arguments. We will receive list of drives from remote machine
                        FilesFromRemoteConn.Send("LIST");
                    }
                    else
                    {
                        // if parentFolder isn't equal to "" , we will send list command with parentFolder as parameter and will receive content of that folder
                        FilesFromRemoteConn.Send("LIST@" + parentFolder1);
                    }                  
                    ContentToDisplay = FilesFromRemoteConn.Receive();
                    //if we received message from remote machine
                    if (ContentToDisplay.Count() == 1 && (ContentToDisplay[0].StartsWith("220") || ContentToDisplay[0].StartsWith("221") || ContentToDisplay[0].StartsWith("222") || ContentToDisplay[0].StartsWith("404")|| ContentToDisplay[0].StartsWith("500")))
                    {
                        //display message inside textbox
                        txtFromServer.Text += ContentToDisplay[0];
                        txtFromServer.Text += Environment.NewLine;
                    }
                    // if we receive a folder content
                    else
                    {
                        WriteContentToListbox();
                    }
                }
                // Local mode
                else
                {
                    ContentToDisplay = LocalFiles.ShowContent(parentFolder1);
                    WriteContentToListbox();
                }
            }
            //last click was made on listbox2
            else if (ChooseListbox == 2)
            {
                parentFolder2 = aux;
                txtparentFolder2.Text = parentFolder2;
                if (ClientMode == 1)
                {
                    if (parentFolder2 == "")
                    {
                        FilesFromRemoteConn.Send("LIST");
                    }
                    else
                    {
                        FilesFromRemoteConn.Send("LIST@" + parentFolder2);
                    }                   
                    ContentToDisplay = FilesFromRemoteConn.Receive();
                    if (ContentToDisplay.Count() == 1 && (ContentToDisplay[0].StartsWith("220") || ContentToDisplay[0].StartsWith("221") || ContentToDisplay[0].StartsWith("222") || ContentToDisplay[0].StartsWith("404") || ContentToDisplay[0].StartsWith("500")))
                    {
                        txtFromServer.Text += ContentToDisplay[0];
                        txtFromServer.Text += Environment.NewLine;
                    }
                    else
                    {
                        WriteContentToListbox();
                    }
                }
                else
                {
                    ContentToDisplay = LocalFiles.ShowContent(parentFolder2);
                    WriteContentToListbox();
                }
            }
        }
        
        //Start listening button click
        private void StartListening_Click(object sender, EventArgs e)
        {
            //Server is stopped so nobody can connect to our machine    
            if (StartListening.Text.Equals("Start Listening"))
            {
                //instantiate new server and start it on new thread
                ShareFiles = new Server();
                ServerThread = new Thread(ShareFiles.Start);
                ServerThread.Start();
                StartListening.Text = "Stop Listening";
            }
            //Server is working and is listening for connections.
            else if (StartListening.Text.Equals("Stop Listening"))
            {
                StartListening.Text = "Start Listening";
            }
        }
        
        //Click on Copy button from ContextMenuStrip
        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //It means that we will copy an item.
            CopyFlag = 1;
            //if move flag was setted before by mistake by user, we will unset it.
            if (MoveFlag == 1)
                MoveFlag = 0;

            //last click was made on listbox 1
            if (ChooseListbox == 1)
            {
                // Set source directory (from which we will copy file)
                LocalFiles.SetSrcDirectory(Path.Combine(parentFolder1, listBox1.SelectedItem.ToString()));
                //set just file name, without absolute path
                LocalFiles.SetFileName(listBox1.SelectedItem.ToString());
                pasteToolStripMenuItem.Enabled = true;
            }
            //last click was made on listbox 2
            else if (ChooseListbox == 2)
            {
                LocalFiles.SetSrcDirectory(Path.Combine(parentFolder2, listBox2.SelectedItem.ToString()));
                LocalFiles.SetFileName(listBox2.SelectedItem.ToString());
                pasteToolStripMenuItem.Enabled = true;
            }

        }

        //Click on Paste button from ContextMenuStrip
        private void pasteToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            //last click was made on listbox1
            if (ChooseListbox == 1)
            {
                //We set here destination directory
                LocalFiles.SetDestDirectory(parentFolder1);
                pasteToolStripMenuItem.Enabled = false;
            }
            //last click was made on listbox2
            else if (ChooseListbox == 2)
            {
                //We set here destination directory
                LocalFiles.SetDestDirectory(parentFolder2);
                pasteToolStripMenuItem.Enabled = false;
            }
            //Check if we are in remote mode
            if (ClientMode == 1)
            {
                //if we are performing copy operation
                if (CopyFlag == 1)
                {
                    //Send to remote machine COPY command with 3 arguments.
                    FilesFromRemoteConn.Send(String.Format("COPY@{0}@{1}@{2}", LocalFiles.GetSrcDirectory(), LocalFiles.GetFileName(), LocalFiles.GetDestDirectory()));
                }
                //if we are performing move operation
                else if (MoveFlag == 1)
                {
                    //Send to remote machine MOVE command with 3 arguments.
                    FilesFromRemoteConn.Send(String.Format("MOVE@{0}@{1}@{2}", LocalFiles.GetSrcDirectory(), LocalFiles.GetFileName(), LocalFiles.GetDestDirectory()));
                }
                ContentToDisplay = FilesFromRemoteConn.Receive();

                //Check if we receive message. Messages starts with 3 digit number
                if (ContentToDisplay.Count() == 1 && (ContentToDisplay[0].StartsWith("220") || ContentToDisplay[0].StartsWith("221") || ContentToDisplay[0].StartsWith("222") || ContentToDisplay[0].StartsWith("404") || ContentToDisplay[0].StartsWith("500")))
                {
                    //Display messages in textbox
                    txtFromServer.Text += ContentToDisplay[0];
                    txtFromServer.Text += Environment.NewLine;
                }
                else
                {
                    //Display folder content
                    WriteContentToListbox();
                }
            }
            //We are in local mode
            else
            {
                // Function for which perform copy operation on local machine
                LocalFiles.CopyMoveOp(CopyFlag, MoveFlag, 0);
               
                //Performing copy operation
                if (CopyFlag == 1)
                {
                    //if last click was made on listbox1 we will display updated folder in listbox1, otherwise in listbox2
                    if (ChooseListbox == 1)
                    {
                        ContentToDisplay = LocalFiles.ShowContent(parentFolder1);
                        WriteContentToListbox();
                    }
                    else if (ChooseListbox == 2)
                    {
                        ContentToDisplay = LocalFiles.ShowContent(parentFolder2);
                        WriteContentToListbox();
                    }
                    
                    //Copy operation is done, unset flag.
                    CopyFlag = 0;
                }

                //Performing move operation
                else if (MoveFlag == 1)
                {
                    //updates both listboxes and unset move flag
                    ChooseListbox = 1;
                    ContentToDisplay = LocalFiles.ShowContent(parentFolder1);
                    WriteContentToListbox();
                    ChooseListbox = 2;
                    ContentToDisplay = LocalFiles.ShowContent(parentFolder2);
                    WriteContentToListbox();
                    MoveFlag = 0;
                }
            }
        }

        //Click on Move button from ContextMenuStrip
        private void moveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MoveFlag = 1;
            //if user set copy flag before, we will unset it
            if (CopyFlag == 1)
                CopyFlag = 0;

            //last click was made on listbox1
            if (ChooseListbox == 1)
            {
                //set source directory and file name
                LocalFiles.SetSrcDirectory(Path.Combine(parentFolder1, listBox1.SelectedItem.ToString()));
                LocalFiles.SetFileName(listBox1.SelectedItem.ToString());
                pasteToolStripMenuItem.Enabled = true;
            }
            //last click was made on listbox2
            else if (ChooseListbox == 2)
            {
                LocalFiles.SetSrcDirectory(Path.Combine(parentFolder2, listBox2.SelectedItem.ToString()));
                LocalFiles.SetFileName(listBox2.SelectedItem.ToString());
                pasteToolStripMenuItem.Enabled = true;
            }
        }

        //Click on Delete button from ContextMenuStrip
        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //last click was made on listbox1
            if(ChooseListbox == 1)
            {
                //check if we are in remote mode
                if (ClientMode == 1)
                {
                    //send delete command with 2 arguments to remote machine
                    FilesFromRemoteConn.Send(String.Format("DELT@{0}@{1}",parentFolder1, listBox1.SelectedItem.ToString() ));
                    ContentToDisplay = FilesFromRemoteConn.Receive();

                    //we received message from remote machine
                    if (ContentToDisplay.Count == 1 && (ContentToDisplay[0].StartsWith("220") || ContentToDisplay[0].StartsWith("221") || ContentToDisplay[0].StartsWith("222") || ContentToDisplay[0].StartsWith("404") || ContentToDisplay[0].StartsWith("500")))
                    {
                        txtFromServer.Text += ContentToDisplay[0];
                        txtFromServer.Text += Environment.NewLine;
                    }
                    //we received content from remote machine
                    else
                    {
                        WriteContentToListbox();
                    }
                }
                //we are in local mode
                else
                {
                    //call function which will delete specified item from local machine
                    LocalFiles.DeleteOp(Path.Combine(parentFolder1, listBox1.SelectedItem.ToString()));
                    ContentToDisplay = LocalFiles.ShowContent(parentFolder1);
                    WriteContentToListbox();
                }
            }
            else if (ChooseListbox == 2)
            {
                if (ClientMode == 1)
                {
                    FilesFromRemoteConn.Send(String.Format("DELT@{0}@{1}", parentFolder2, listBox2.SelectedItem.ToString()));
                    ContentToDisplay = FilesFromRemoteConn.Receive();
                    if (ContentToDisplay.Count == 1 && (ContentToDisplay[0].StartsWith("220") || ContentToDisplay[0].StartsWith("221") || ContentToDisplay[0].StartsWith("222") || ContentToDisplay[0].StartsWith("404") || ContentToDisplay[0].StartsWith("500")))
                    {
                        txtFromServer.Text += ContentToDisplay[0];
                        txtFromServer.Text += Environment.NewLine;
                    }
                    else
                    {
                        WriteContentToListbox();
                    }
                }
                else
                {
                    LocalFiles.DeleteOp(Path.Combine(parentFolder2, listBox2.SelectedItem.ToString()));
                    ContentToDisplay = LocalFiles.ShowContent(parentFolder2);
                    WriteContentToListbox();
                }
            }
        }

        //Click on Add File button from ContextMenuStrip
        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //insert new file name inside input box
            string newFileName = Microsoft.VisualBasic.Interaction.InputBox("Introduce file name here:", "");
            //invalid file name was introduced
            if (newFileName == "")
            {
                MessageBox.Show("File name should containt at least one character");
            }
            //valid file name was introduced
            else
            {
                //add extension to filename
                newFileName += ".txt";
                //check if last click was made on listbox 1
                if (ChooseListbox == 1)
                {
                    //remote mode is open
                    if (ClientMode == 1)
                    {
                        //Send command which create file
                        FilesFromRemoteConn.Send(String.Format("CRFL@{0}@{1}", parentFolder1, newFileName));
                        ContentToDisplay = FilesFromRemoteConn.Receive();

                        //Server sent message
                        if (ContentToDisplay.Count == 1 && (ContentToDisplay[0].StartsWith("220") || ContentToDisplay[0].StartsWith("221") || ContentToDisplay[0].StartsWith("222") || ContentToDisplay[0].StartsWith("404") || ContentToDisplay[0].StartsWith("500")))
                        {
                            txtFromServer.Text += ContentToDisplay[0];
                            txtFromServer.Text += Environment.NewLine;
                        }
                        //Server sent folder content
                        else
                        {
                            WriteContentToListbox();
                        }
                    }
                    else
                    {
                        //function which create file on local machine in specified directory(parentFolder1)
                        LocalFiles.AddFile(parentFolder1, newFileName);
                        ContentToDisplay = LocalFiles.ShowContent(parentFolder1);
                        WriteContentToListbox();
                    }
                }
                else if (ChooseListbox == 2)
                {
                    if (ClientMode == 1)
                    {
                        FilesFromRemoteConn.Send(String.Format("CRFL@{0}@{1}", parentFolder2, newFileName));
                        ContentToDisplay = FilesFromRemoteConn.Receive();
                        if (ContentToDisplay.Count == 1 && (ContentToDisplay[0].StartsWith("220") || ContentToDisplay[0].StartsWith("221") || ContentToDisplay[0].StartsWith("222") || ContentToDisplay[0].StartsWith("404") || ContentToDisplay[0].StartsWith("500")))
                        {
                            txtFromServer.Text += ContentToDisplay[0];
                            txtFromServer.Text += Environment.NewLine;
                        }
                        else
                        {
                            WriteContentToListbox();
                        }
                    }
                    else
                    {
                        LocalFiles.AddFile(parentFolder2, newFileName);
                        ContentToDisplay = LocalFiles.ShowContent(parentFolder2);
                        WriteContentToListbox();
                    }
                }
            }
        }

        //Click on Add Folder button from ContextMenuStrip
        private void folderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //introduce name for new folder
            string newFileName = Microsoft.VisualBasic.Interaction.InputBox("Introduce file name here:", "");

            //name is invalid
            if (newFileName == "")
            {
                MessageBox.Show("File name should containt at least one character");
            }

            //name is valid
            else
            {
                //last click was made in listbox1
                if (ChooseListbox == 1)
                {
                    //remote mode in on
                    if (ClientMode == 1)
                    {
                        //Send command for creating new directory to remote machine 
                        FilesFromRemoteConn.Send(String.Format("CRDI@{0}@{1}", parentFolder1, newFileName));
                        ContentToDisplay = FilesFromRemoteConn.Receive();

                        //received message
                        if (ContentToDisplay.Count == 1 && (ContentToDisplay[0].StartsWith("220") || ContentToDisplay[0].StartsWith("221") || ContentToDisplay[0].StartsWith("222") || ContentToDisplay[0].StartsWith("404") || ContentToDisplay[0].StartsWith("500")))
                        {
                            txtFromServer.Text += ContentToDisplay[0];
                            txtFromServer.Text += Environment.NewLine;
                        }
                        else
                        {
                            WriteContentToListbox();
                        }
                    }

                    //local mode
                    else
                    {
                        //Call function which creates directory on local machine and display it.
                        LocalFiles.AddDirectory(parentFolder1, newFileName);
                        ContentToDisplay = LocalFiles.ShowContent(parentFolder1);
                        WriteContentToListbox();
                    }
                }
                //last click was made on listbox2. Everything is the same like listbox1
                else if (ChooseListbox == 2)
                {
                    if (ClientMode == 1)
                    {
                        FilesFromRemoteConn.Send(String.Format("CRDI@{0}@{1}", parentFolder2, newFileName));
                        ContentToDisplay = FilesFromRemoteConn.Receive();
                        if (ContentToDisplay.Count == 1 && (ContentToDisplay[0].StartsWith("220") || ContentToDisplay[0].StartsWith("221") || ContentToDisplay[0].StartsWith("222") || ContentToDisplay[0].StartsWith("404") || ContentToDisplay[0].StartsWith("500")))
                        {
                            txtFromServer.Text += ContentToDisplay[0];
                            txtFromServer.Text += Environment.NewLine;
                        }
                        else
                        {
                            WriteContentToListbox();
                        }
                    }
                    else
                    {
                        LocalFiles.AddDirectory(parentFolder2, newFileName);
                        ContentToDisplay = LocalFiles.ShowContent(parentFolder2);
                        WriteContentToListbox();
                    }
                }
            }
        }

        //Click on Rename button from ContextMenuStrip
        private void renameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //introduce new name for folder/file
            string newName = Microsoft.VisualBasic.Interaction.InputBox("Introduce new name here:");

            //name is invalid
            if (newName.Equals(""))
            {
                MessageBox.Show("invalid new name for file/directory");
            }
            //name is valid
            else
            {
                //last click was made on listbox1
                if (ChooseListbox == 1)
                {
                    //remote mode is on
                    if (ClientMode == 1)
                    {
                        //Send command to remote machine
                        FilesFromRemoteConn.Send(String.Format("RENM@{0}@{1}@{2}", parentFolder1, listBox1.SelectedItem.ToString(), newName));
                        ContentToDisplay = FilesFromRemoteConn.Receive();
                        //received message
                        if (ContentToDisplay.Count == 1 && (ContentToDisplay[0].StartsWith("220") || ContentToDisplay[0].StartsWith("221") || ContentToDisplay[0].StartsWith("222") || ContentToDisplay[0].StartsWith("404") || ContentToDisplay[0].StartsWith("500")))
                        {
                            txtFromServer.Text += ContentToDisplay[0];
                            txtFromServer.Text += Environment.NewLine;
                        }
                        else
                        {
                            //received content
                            WriteContentToListbox();
                        }
                    }
                    //local mode
                    else
                    {
                        //Call function which renames file/folder on local machine
                        LocalFiles.RenameOp(parentFolder1, listBox1.SelectedItem.ToString(), newName);
                        ContentToDisplay = LocalFiles.ShowContent(parentFolder1);
                        WriteContentToListbox();
                    }
                }
                //last click was made on listbox2 . Everything is the same like listbox1
                else if (ChooseListbox == 2)
                {
                    if (ClientMode == 1)
                    {
                        FilesFromRemoteConn.Send(String.Format("RENM@{0}@{1}@{2}", parentFolder2, listBox2.SelectedItem.ToString(), newName));
                        ContentToDisplay = FilesFromRemoteConn.Receive();
                        if (ContentToDisplay.Count == 1 && (ContentToDisplay[0].StartsWith("220") || ContentToDisplay[0].StartsWith("221") || ContentToDisplay[0].StartsWith("222") || ContentToDisplay[0].StartsWith("404") || ContentToDisplay[0].StartsWith("500")))
                        {
                            txtFromServer.Text += ContentToDisplay[0];
                            txtFromServer.Text += Environment.NewLine;
                        }
                        else
                        {
                            WriteContentToListbox();
                        }
                    }
                    else
                    {
                        LocalFiles.RenameOp(parentFolder2, listBox2.SelectedItem.ToString(), newName);
                        ContentToDisplay = LocalFiles.ShowContent(parentFolder2);
                        WriteContentToListbox();
                    }
                }
            }
        }

        //Connect button Click
        private void btnConnect_Click(object sender, EventArgs e)
        {
            //Local mode is on. 
            if (btnConnect.Text.Equals("Connect"))          
            {
                //Check every field is not empty. If it's empty we don't have enough information for connecting to remote computer
                if (!txtIP.Text.Equals("") && !txtPort.Text.Equals("") && !txtPass.Text.Equals(""))
                {
                    //instantiate class which manages communication with remote station
                    FilesFromRemoteConn = new Client(txtIP.Text, Convert.ToInt32(txtPort.Text));
                    //Send password for logging in
                    FilesFromRemoteConn.Send(txtPass.Text);
                    ContentToDisplay = FilesFromRemoteConn.Receive();

                    //Wrong password was sent. We have to try logging in with new password
                    while (ContentToDisplay[0].StartsWith("222"))
                    {
                        txtFromServer.Text += ContentToDisplay[0];
                        txtFromServer.Text += Environment.NewLine;
                        string password = Microsoft.VisualBasic.Interaction.InputBox("Introduce password one more time");
                        FilesFromRemoteConn.Send(password);
                        ContentToDisplay = FilesFromRemoteConn.Receive();
                    }
                    txtFromServer.Text += ContentToDisplay[0];
                    txtFromServer.Text += Environment.NewLine;

                    //Wrong password was sent too many times. Connection is closing
                    if (ContentToDisplay[0].StartsWith("404"))
                    {
                        FilesFromRemoteConn.CloseConnection();
                    }

                    //We sent good password. Welcome message starts with 220
                    else if (ContentToDisplay[0].StartsWith("220"))
                    {
                        //Enable remote mode
                        ClientMode = 1;
                        ChooseListbox = 0;

                        //Send list command without parameters. It means that server will send us drive names
                        FilesFromRemoteConn.Send("LIST");
                        ContentToDisplay = FilesFromRemoteConn.Receive();

                        //Display remote drives
                        WriteContentToListbox();
                        btnConnect.Text = "Disconnect";
                    }
                }
                else
                {
                    MessageBox.Show("One field is not valid");
                }
            }

            //We are connected to remote machine and we intend to interrupt connection
            else if (btnConnect.Text.Equals("Disconnect"))
            {
                btnConnect.Text = "Connect";

                //Send QUIT command
                FilesFromRemoteConn.Send("QUIT");

                //Receive bye bye message
                ContentToDisplay = FilesFromRemoteConn.Receive();
                txtFromServer.Text += ContentToDisplay[0];
                txtFromServer.Text += Environment.NewLine;
                //Closing connection
                FilesFromRemoteConn.CloseConnection();

                //Enable local mode and show local drives
                ClientMode = 0;
                ContentToDisplay = LocalFiles.ShowContent("");
                parentFolder1 = "";
                parentFolder2 = "";
                ChooseListbox = 0;
                WriteContentToListbox();
            }
        }

        //Close application Click
        private void DisplayFiles_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Remote mode is on
            if (ClientMode == 1)
            {
                //Enable local mode, disable remote one
                ClientMode = 0;

                //Send quit command to server
                FilesFromRemoteConn.Send("QUIT");
                //receive message and display it in proper textbox
                ContentToDisplay = FilesFromRemoteConn.Receive();
                txtFromServer.Text += ContentToDisplay[0];
                txtFromServer.Text += Environment.NewLine;
                Thread.Sleep(100);
                //closing connection
                FilesFromRemoteConn.CloseConnection();
            }

            //Check if we are listening to other connections 
            if (StartListening.Text == "Stop Listening")
            {
                //interrupting connections with all clients who are connected to my computer
                for (int i = 0; i < Server.ConnectedClients.Count; i++)
                {
                    Server.DeleteClientFromList(Server.ConnectedClients[i]);
                }
                //Stop listening for connections
                ShareFiles.Listener.Close();
            }
            //Close application
            Application.Exit();
        }
    }
}
