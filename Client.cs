﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileManagerV1._0
{
    class Client
    {
        //Through whis socket we will send and receive data and messages to remote message
        Socket ConnectToServer;
        //Ip address of remote machine
        IPAddress remoteIpAddress;
        IPEndPoint remoteEndPoint;

        public Client(string ipAddress, int port)
        {
            try
            {
                ConnectToServer = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                ConnectToServer.Blocking = true;
                remoteIpAddress = IPAddress.Parse(ipAddress);
                remoteEndPoint = new IPEndPoint(remoteIpAddress, port);
                //Connect to remote machine
                ConnectToServer.Connect(remoteEndPoint);
            }
            catch(Exception ex)
            {
                //remote machine does not start server
                MessageBox.Show("This client is unavailable or connection is lost");
            }
        }

        public void Send(string msg)
        {
            try
            {
                //Encode data into bytes and send it to server
                byte[] data = Encoding.ASCII.GetBytes(msg);
                ConnectToServer.Send(data);
            }
            catch (Exception e)
            {
                //Suddenly, connection was closed by server
                MessageBox.Show("This client is unavailable or connection is lost");
            }
        }

        //This function returns list of strings which represents folder content. If we received a single message we will return a list with just one item inside
        public List<string> Receive()
        {
            string line = null;
            List<string> response = new List<string>();
            try
            {
                do
                {                   
                    byte[] buffer = new byte[1024];
                    //received something from server
                    int aux = ConnectToServer.Receive(buffer);
                    char[] msg = new char[aux];

                    Decoder d = Encoding.UTF8.GetDecoder();
                    //decoding message into characters
                    int charLen = d.GetChars(buffer, 0, aux, msg, 0);
                    //create string from decoded characters
                    line = new System.String(msg);
                    if (!line.Equals("!"))
                    {
                        response.Add(line);
                    }
                    //We will receive data until server won't send us !
                } while (!line.Equals("!"));
                //return received message
                return response;
            }
            //Connection was interrupted while server was sending data to us
            catch(Exception ex)
            {
                //Clear response buffer
                response.Clear();
                response.Add("500 Internal Error. Connection lost");
                return response;
            }
        }

        public void CloseConnection()
        {
            //Close connection
            ConnectToServer.Close();
        }
    }
}
