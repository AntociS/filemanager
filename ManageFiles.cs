﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace FileManagerV1._0
{
    //This class manages local files
    class ManageFiles
    {
       private string SrcDirectory;
       private string DestDirectory;
       private string FileName;
       private string SrcDirectoryRemote;
       private string DestDirectoryRemote;
       private string FileNameRemote;

        private int AllowContent; 

        public ManageFiles()
        {
            SrcDirectory = "";
            DestDirectory = "";
            FileName = "";
            SrcDirectoryRemote = "";
            DestDirectoryRemote = "";
            FileNameRemote = "";
            AllowContent = 1;
        }

        /*
         * We have 2 types of getters: Local and remote setters or getters
         * Remote setters and getters are used then operation is performed by connected client to server
         * Local setters and getters are used for operations performed on local machine by owner
         * I don't want to use same getters and setters for local and remote modes because owner and connected client to server can make changes in the same time
         */

        public void SetSrcDirectoryRemote(string Directory)
        {
            SrcDirectoryRemote = Directory;
        }

        public string GetSrcDirectoryRemote()
        {
            return SrcDirectoryRemote;
        }

        public void SetDestDirectoryRemote(string Directory)
        {
            DestDirectoryRemote = Directory;
        }
        
        public string GetDestDirectoryRemote()
        {
            return DestDirectoryRemote;
        }

        public void SetFileNameRemote(string FileName)
        {
            FileNameRemote = FileName;
        }

        public string GetFileNameRemote()
        {
            return FileNameRemote;
        }

        public void SetSrcDirectory(string Directory)
        {
            SrcDirectory = Directory;
        }

        public string GetSrcDirectory()
        {
            return SrcDirectory;
        }

        public string GetFileName()
        {
            return FileName;
        }

        public void SetFileName(string Filen)
        {
            FileName = Filen;
        }

        public void SetDestDirectory(string Directory)
        {
            DestDirectory = Directory;
        }

        public string GetDestDirectory()
        {
            return DestDirectory;
        }

        //Function which returns content of a directory
        public List<string> ShowContent(string path)
        {
            List<string> FolderContent = new List<string>();

            //if argument is "" we will display list of drives
            if (path == "")
            {
                //Get drives from local machine
                DriveInfo[] drives = DriveInfo.GetDrives();
                foreach (DriveInfo d in drives)
                {
                    if (d.IsReady)
                    {

                        //add drive names to a list
                        FolderContent.Add(d.Name);
                    }
                }
            }
            //argument is not ""
            else
            {

                DirectoryInfo Folder = new DirectoryInfo(path);
                try
                {
                    //Check if folder exists
                    if (Folder.Exists)
                    {
                        //Enumerate all folders and files from specified folder and concatenate them
                        IEnumerable<string> Directories = Directory.EnumerateDirectories(path);
                        IEnumerable<string> Files = Directory.EnumerateFiles(path);
                        IEnumerable<string> Content = Directories.Concat(Files);
                        foreach (string dir in Content)
                        {
                            AllowContent = 1;
                            //Check if directory is reparse point. We won't send to client reparse points
                            if (Directory.Exists(dir))
                            {
                                FileAttributes attr = File.GetAttributes(dir);
                                if ( (attr & FileAttributes.ReparsePoint) == FileAttributes.ReparsePoint)
                                {
                                    AllowContent = 0;
                                }
                            }
                            //Directory is not reparse point so we can send it to client
                            if (AllowContent == 1)
                            {
                                //We want to display content of a drive
                                if (path.Length == 3)
                                {
                                    //we won't send to client System Volume Information folder
                                    if (dir.Substring(path.Length) != "System Volume Information")
                                    {
                                        FolderContent.Add(dir.Substring(path.Length));
                                    }                                  
                                }
                                else
                                {
                                    FolderContent.Add(dir.Substring(path.Length + 1));
                                }
                            }
                        }
                    }
                }
           
                catch(UnauthorizedAccessException e)
                {
                    MessageBox.Show(e.ToString());
                }
            }
            //Sort alphabetically content of folder and return it
            FolderContent.Sort();
            return FolderContent;
        }
        
        //Copy/move operation
        public void CopyMoveOp(int CopyFlag, int MoveFlag, int RemoteFlag)
        {
            //Check if remote flag is on
            if (RemoteFlag == 1)
            {
                //Check if selected item is file
                if (File.Exists(SrcDirectoryRemote))
                {
                    //Check if file with the same name isn't in destination directory
                    if (!File.Exists(Path.Combine(DestDirectoryRemote, FileNameRemote)))
                    {
                        //Check if user asking for copy operation
                        if (CopyFlag == 1)
                        {
                            //Copy file to dest directory
                            File.Copy(SrcDirectoryRemote, Path.Combine(DestDirectoryRemote, FileNameRemote));
                        }
                        //Check if user asking for move operation
                        else if (MoveFlag == 1)
                        {
                            //Move file to dest directory
                            File.Move(SrcDirectoryRemote, Path.Combine(DestDirectoryRemote, FileNameRemote));
                        }
                    }
                }

                //Check if selected item is directory
                else if (Directory.Exists(SrcDirectoryRemote))
                {
                    //Check if selected directory isn't in destination directory
                    if (SrcDirectoryRemote != Path.Combine(DestDirectoryRemote, FileNameRemote))
                    {
                        if (CopyFlag == 1)
                        {
                            //Copy directory function
                            CopyDirectory(SrcDirectoryRemote, Path.Combine(DestDirectoryRemote, FileNameRemote), true);
                        }
                        else if (MoveFlag == 1)
                        {
                            //move operation
                            Microsoft.VisualBasic.FileIO.FileSystem.MoveDirectory(SrcDirectoryRemote, Path.Combine(DestDirectoryRemote, FileNameRemote));
                        }
                    }
                }
            }

            //Local mode is on. Same operation as remote mode
            else
            {
                if (File.Exists(SrcDirectory))
                {
                    if (File.Exists(Path.Combine(DestDirectory, FileName)))
                    {
                        DialogResult dialogresult = MessageBox.Show("Do you allow files overwriting?", "", MessageBoxButtons.YesNo);
                        if (dialogresult == DialogResult.Yes)
                        {
                            if (CopyFlag == 1)
                            {
                                File.Copy(SrcDirectory, Path.Combine(DestDirectory, FileName), true);
                            }
                            else if (MoveFlag == 1)
                            {
                                File.Move(SrcDirectory, Path.Combine(DestDirectory, FileName));
                            }
                        }
                    }
                    else
                    {
                        if (CopyFlag == 1)
                        {
                            File.Copy(SrcDirectory, Path.Combine(DestDirectory, FileName));
                        }
                        else if (MoveFlag == 1)
                        {

                            File.Move(SrcDirectory, Path.Combine(DestDirectory, FileName));
                        }
                    }
                }
                else if (Directory.Exists(SrcDirectory))               
                {

                    if (SrcDirectory != Path.Combine(DestDirectory, FileName))
                    {
                        if (CopyFlag == 1)
                        {
                            CopyDirectory(SrcDirectory, Path.Combine(DestDirectory, FileName), true);
                        }
                        else if (MoveFlag == 1)
                        {
                            Microsoft.VisualBasic.FileIO.FileSystem.MoveDirectory(SrcDirectory, Path.Combine(DestDirectory, FileName));
                            //Directory.Move(SrcDirectory, Path.Combine(DestDirectory, FileName));
                        }
                    }
                }
            }
        }

        //Delete operation
        public void DeleteOp(string path)
        {
            //check if selected item is file
            if (File.Exists(path))
            {
                //Delete file from specified path
                File.Delete(path);
            }
            //check if selected item is directory
            else if (Directory.Exists(path))
            {
                //Delete directory from specified path
                Directory.Delete(path, true);
            }
        }

        //Copy Directory function
        public void CopyDirectory(string sDirectory, string dDirectory, bool copySubDirs)
        {
            DirectoryInfo dir = new DirectoryInfo(sDirectory);
            //Get all directories from source directory
            DirectoryInfo[] dirs = dir.GetDirectories();

            //Check if destination directory exists
            if (!Directory.Exists(dDirectory))
            {
                // If it does not exists, we will create it
                Directory.CreateDirectory(dDirectory);
            }

            //Get all files from source directory
            FileInfo[] files = dir.GetFiles();

            //Copy all files to new directory
            foreach(FileInfo file in files)
            {
                string newpath = Path.Combine(dDirectory, file.Name);
                file.CopyTo(newpath, false);
            }
            //flag which allows copying all subdirectories
            if(copySubDirs)
            {
                //Copy subdirectories to new location
                foreach(DirectoryInfo subdir in dirs)
                {
                    string newpath = Path.Combine(dDirectory, subdir.Name);
                    CopyDirectory(subdir.FullName, newpath, true);
                }
            }
        }

        //Create file
        public void AddFile(string path, string newFile)
        {
            //create new file at specified path
                using (File.Create(Path.Combine(path, newFile))) { }
        }

        //Create directory
        public void AddDirectory(string path, string newDirectory)
        {
            try
            {
                //create new directory at specified path
                    Directory.CreateDirectory(Path.Combine(path, newDirectory));
            }
            catch(Exception )
            {
                MessageBox.Show("Something is wrong. Check if you have permission to create folder or if folder with the same name does not already exists");
            }

        }

        //rename operation
        public void RenameOp(string path, string oldPath, string newPath)
        {
            try
            {
                string extension = "";
                //Check if specified file exists
                if (File.Exists(Path.Combine(path, oldPath)))
                {
                    //extract extension from old path and add it to new path
                    extension = Path.GetExtension(Path.Combine(path, oldPath));
                    newPath += extension;
                }
                //Rename file
                FileSystem.Rename(Path.Combine(path, oldPath), Path.Combine(path, newPath));
            }
            catch( ArgumentException )
            {
                MessageBox.Show("Invalid Path!");
            }
            catch( IOException )
            {
                MessageBox.Show("File with the same name already exists or you can't perform this operation on this file");
            }
        }
    }
}
