﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Windows.Forms;

namespace FileManagerV1._0
{

    public class Server
    {
       public Socket  Listener;
        Socket Client;
        IPEndPoint ipLocal;
        ClientConnection IncomingClient;
        public static List<Socket> ConnectedClients;
        int nrOfClients = 0;


        public Server()
        {
            ConnectedClients = new List<Socket>();
            Listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            Listener.Blocking = true;
            ipLocal = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 21);
            Listener.Bind(ipLocal);
        }

        //Start server
        public void Start()
        {
            //Server can accept just 10 clients in the same time
            Listener.Listen(10);
            try
            {

                //waiting for connections
                while (true)
                {
                    Client = Listener.Accept();
                    ConnectedClients.Add(Client);
                    //make send and receive methods blockants
                    Client.Blocking = true;
                    //instantiate new ClientConnection class
                    IncomingClient = new ClientConnection(Client);
                    //Start new connection on another thread
                    IncomingClient.Start();
                }
            }
            catch(Exception ex)
            {

            }

        }

        //delete specified client from list of clients
        public static void DeleteClientFromList(Socket clnt)
        {
            for (int i = 0; i < ConnectedClients.Count;i++)
            {
                if(ConnectedClients[i] == clnt)
                {
                    ConnectedClients.RemoveAt(i);
                }
            }
        }
    }
}
